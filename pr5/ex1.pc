/*
 * Pro*C
 * TP5 - Exercice 1
 *
 * Auteur : Damien NGUYEN
 * Groupe : 3
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

EXEC SQL INCLUDE SQLCA.H;
EXEC SQL INCLUDE SQLDA.H;
EXEC SQL INCLUDE ORACA.H;

#define CLEAR       system("clear")

/********** Constantes **********/
#define SQL_COMMIT      1
#define STOP            1
#define CONTINUE        0
#define SQL_ROLLBACK    0
#define SQL_SUCCESS     0

/********** Liste des fonctions **********/
void connexion(void);
void erreur_sql(int arret);
void deconnexion(int validation);

/*
 * Fonction de connexion à Oracle.
 *
 * Les identifiants sont rentrés en dur pour ne pas avoir à les retaper
 * à la main à chaque test, mais dans l'idéal, on demande à l'utilisateur
 * de les rentrer et on concatène les identifiants à uid.arr.
 */
void connexion(void) {
    VARCHAR uid[50];
    char identifiants[30] = "danguyen1/danguyen1@kirov";
    printf("Connexion avec les identifiants suivants : %s .\n", identifiants);
    strcpy(uid.arr, identifiants);
    uid.len = strlen(uid.arr);

    EXEC SQL CONNECT :uid;
    if (sqlca.sqlcode == SQL_SUCCESS) {
        printf("Connexion réussie avec succès !\n\n");
    }
    else {
        printf("Connexion échouée !\n\n");
        exit(EXIT_FAILURE);
    }
}

/*
 * Fonction qui affiche les code et message d'erreur SQL.
 *
 * Paramètres :
 *      arret       STOP(1) pour quitter, n'importe quoi pour continuer
 */
void erreur_sql(int arret) {
    printf("Code d'erreur : %d.\n", sqlca.sqlcode);
    printf("Message erreur : %.*s.\n", sqlca.sqlerrm.sqlerrml, sqlca.sqlerrm.sqlerrmc);
    if (arret == STOP) {
        deconnexion(SQL_ROLLBACK);
        exit(EXIT_FAILURE);
    }
}

/*
 * Fonction de déconnexion.
 *
 * Paramètres :
 *      validation  SQL_COMMIT(1) pour COMMIT, n'importe quoi pour ROLLBACK
 */
void deconnexion(int validation) {
    if (validation == 1) {
        EXEC SQL COMMIT WORK RELEASE;
    }
    else {
        EXEC SQL ROLLBACK WORK RELEASE;
    }
    printf("Déconnexion réussie, travail %s.\n", validation == 1 ? "enregistré" : "annulé");
}

/*
 * Fonction principale.
 *
 * Paramètres :
 *      argc        Le nombre d'arguments
 *      argv        Le tableau d'arguments
 *
 * Retourne :
 *      le code de retour défini dans stdlib.h
 *      (EXIT_SUCCESS = 0)
 */
int main(int argc, char const *argv[]) {
    char deptimm[3], noimm[5];
    int validation;
    VARCHAR adrimm[81];

    CLEAR;

    connexion();

    printf("Saisir le numéro d'immeuble : ");
    scanf("%s%*c", noimm);
    printf("Saisir la nouvelle adresse de l'immeuble : ");
    fgets(adrimm.arr, sizeof (adrimm.arr), stdin);
    if (adrimm.arr[strlen(adrimm.arr) - 1] == '\n') {
        adrimm.arr[strlen(adrimm.arr) - 1] = '\0';
    }
    adrimm.len = strlen(adrimm.arr);
    printf("Saisir le nouveau numéro de département : ");
    scanf("%s%*c", deptimm);

    EXEC SQL
        INSERT INTO TIMM2018
        VALUES(:noimm, :adrimm, :deptimm);
    if (sqlca.sqlcode < SQL_SUCCESS) {
        erreur_sql(STOP);
    }

    EXEC SQL COMMIT;
    if (sqlca.sqlcode < SQL_SUCCESS) {
        erreur_sql(STOP);
    }

    printf("Enregistré avec succès.\n");

    printf("Enregistrer le travail ? (1 = oui, * = non)\n");
    scanf("%d%*c", &validation);
    deconnexion(validation);

    return EXIT_SUCCESS;
}

/****************************************************************************************************
Cas où l'immeuble existe déjà (ne marche pas)
****************************************************************************************************
danguyen1@iutclinfa1910:~/S2/Database/pr5$ ./ex1

Connexion avec les identifiants suivants : danguyen1/danguyen1@kirov .
Connexion réussie avec succès !

Saisir le numéro d'immeuble : I001
Saisir la nouvelle adresse de l'immeuble : Ne marche pas car existe
Saisir le nouveau numéro de département : 21
Code d'erreur : -1.
Message erreur : ORA-00001: unique constraint (DANGUYEN1.SYS_C00503354) violated
Déconnexion réussie, travail annulé.
****************************************************************************************************/

/****************************************************************************************************
Cas où l'insertion fonctionne
****************************************************************************************************
danguyen1@iutclinfa1910:~/S2/Database/pr5$ ./ex1

Connexion avec les identifiants suivants : danguyen1/danguyen1@kirov .
Connexion réussie avec succès !

Saisir le numéro d'immeuble : I123
Saisir la nouvelle adresse de l'immeuble : Cela fonctionne
Saisir le nouveau numéro de département : 63
Enregistré avec succès.
Enregistrer le travail ? (1 = oui, * = non)
1
Déconnexion réussie, travail enregistré.
****************************************************************************************************
Dans SQLPLUS
****************************************************************************************************
SQL> SELECT * FROM TIMM2018;

NOIM ADRIMM                                       DE
---- -------------------------------------------------------------------------------- --
I001 Bd Lafayette Clermont Fd                                 63
I002 Bd de Courtais Montlucon                                 03
I003 Bd Lafayette Clermont Fd                                 63
I123 Cela fonctionne                                          63
****************************************************************************************************/
