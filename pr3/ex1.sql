---------------------------------------- Exercice 1 ----------------------------------------
set echo off;
set verify off;
set feed off;

@../part1.sql
PROMPT Le fichier part1.sql a ete recharge

DROP TABLE TLIGNE;
CREATE TABLE TLIGNE(LIGNE VARCHAR2(500));
PROMPT TLIGNE nettoyee et cree...

VARIABLE vnumcli	CHAR(6)
PROMPT Saisir le numero client ? 
ACCEPT vnumcli

DECLARE

    dnbcli          NUMBER;
    dnbloc          NUMBER;

    dnoloc          TLOCATION2018.NOLOC%type;
    dreflog         TLOCATION2018.REFLOG%type;
    dfinloc         TLOCATION2018.FINLOC%type;

    dnbprop         NUMBER;

    cli_inconnu     EXCEPTION;
    pas_loc         EXCEPTION;
    cli_is_prop     EXCEPTION;

    CURSOR c_loc IS
        SELECT NOLOC, REFLOG, FINLOC
            FROM TLOCATION2018
            WHERE NOLOC = '&vnumcli'
        FOR UPDATE;

    CURSOR c_log IS
        SELECT REFLOG
            FROM TLOGT2018
            WHERE NOLOC = '&vnumcli'
        FOR UPDATE;

BEGIN
	
    -- On teste déjà si le client existe
	SELECT COUNT(*)
        INTO dnbcli
        FROM TCLIENT2018
        WHERE NOCLI = '&vnumcli';
    IF dnbcli > 0 THEN
        -- On regarde déjà si la personne a des locations en cours ou passées avant de supprimer
        SELECT COUNT(*)
            INTO dnbloc
            FROM TLOCATION2018
            WHERE NOLOC = '&vnumcli';
        IF dnbloc > 0 THEN
            -- On vérifie que la personne ne soit PAS propriétaire pour pouvoir supprimer
            SELECT COUNT(NOPROP)
                INTO dnbprop
                FROM TLOGT2018
                WHERE NOPROP = '&vnumcli';
            IF dnbprop > 0 THEN
                RAISE cli_is_prop;
            END IF;

            -- Suppression dans TLOCATION2018
            OPEN c_loc;
            FETCH c_loc INTO dnoloc, dreflog, dfinloc;

            WHILE c_loc%FOUND LOOP
                DELETE FROM TLOCATION2018 WHERE CURRENT OF c_loc;

                -- On regarde s'il s'agissait d'un emprunt EN COURS (juste pour l'affichage)
                IF dfinloc IS NULL THEN
                    INSERT INTO TLIGNE VALUES('Client supprime en tant que locataire du logement : ' || dreflog || '.');
                ELSE
                    INSERT INTO TLIGNE VALUES('Client supprime en tant qu ancien locataire.');
                END IF;

                FETCH c_loc INTO dnoloc, dreflog, dfinloc;
            END LOOP;
            CLOSE c_loc;

            -- On remet le numero du locataire à NULL et l'état à disponible pour la location
            OPEN c_log;
            FETCH c_log INTO dreflog;

            WHILE c_log%FOUND LOOP
                UPDATE TLOGT2018
                    SET NOLOC = NULL, ETAT = 'D'
                    WHERE CURRENT OF c_log;
                FETCH c_log INTO dreflog;
            END LOOP;
            CLOSE c_log;

            -- Une fois que tout s'est bien passé on peut virer le client de TCLIENT2018
            -- Tout devrait bien se passer, comme il s'agit d'une clé primaire
            DELETE FROM TCLIENT2018 WHERE NOCLI = '&vnumcli';
            INSERT INTO TLIGNE VALUES('Client supprime.');
        ELSE
            RAISE pas_loc;
        END IF;
    ELSE
        RAISE cli_inconnu;
    END IF;

EXCEPTION
    WHEN cli_inconnu THEN
        INSERT INTO TLIGNE
            VALUES('Client inconnu !');
    WHEN pas_loc THEN
        INSERT INTO TLIGNE
            VALUES('Client n ayant aucune location en cours ou ancienne !');
    WHEN cli_is_prop THEN
        INSERT INTO TLIGNE
            VALUES('Client proprietaire, suppression impossible !');

END;

---------------------------------------- Terminal output ----------------------------------------
/*
================================================================================
Cas où c'est un succès
================================================================================
Saisir le numero client ?
CL0002

LIGNE
--------------------------------------------------------------------------------
Client supprime en tant que locataire du logement : L002.
Client supprime en tant que locataire du logement : L005.
Client supprime en tant qu ancien locataire.
Client supprime en tant qu ancien locataire.
Client supprime.
SQL> SELECT * FROM TCLIENT2018;

NOCLI  NOMCLI
------ --------------------
CL0001 Dupond
CL0003 Thibaut
CL0004 Durand

3 rows selected.
*/

/*
================================================================================
Cas où le client est inconnu
================================================================================
Saisir le numero client ?
CLiuiu

LIGNE
--------------------------------------------------------------------------------
Client inconnu !
*/

/*
================================================================================
Cas où le client est propriétaire et ne peut pas être supprimé
================================================================================
Saisir le numero client ?
CL0001

LIGNE
--------------------------------------------------------------------------------
Client proprietaire, suppression impossible !
*/

.
/

SELECT * FROM TLIGNE;

set verify on;
set feed on;
set echo on;
