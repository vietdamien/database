---------------------------------------- Exercice 2 ----------------------------------------
set echo off;
set verify off;
set feed off;

--ALTER SESSION set NLS_DATE_FORMAT='DD/MM/YYYY HH24:MI:SS';

@../part1.sql
PROMPT Le fichier part1.sql a ete recharge

SET PAGESIZE 200;
SET LINESIZE 200;

INSERT INTO TIMM2018 VALUES('I004', 'Bd Lafayette Clermont Fd', '91');
PROMPT On un nouvel immeuble dans le departement 91 pour tester...

DROP TABLE TLIGNE;
CREATE TABLE TLIGNE(LIGNE VARCHAR2(140));
PROMPT TLIGNE nettoyee et cree...

DROP TABLE TRESULT;
CREATE TABLE TRESULT
(
    REFLOG              CHAR(4),
    ETAT                VARCHAR2(20),
    ADRIMM              VARCHAR2(40),
    NOMPROP             VARCHAR2(30),
    NBLOCS              VARCHAR2(40)
);
PROMPT TRESULT nettoyee et cree...

VARIABLE vdeptimm		CHAR(2)
PROMPT Saisir le numero departement ?
ACCEPT vdeptimm

DECLARE

    dcheckdept      TIMM2018.DEPTIMM%type;

    -- Pour l'affichage
    dreflog         TLOCATION2018.REFLOG%type;
    detat           TLOGT2018.ETAT%type;
    dnewEtat        TRESULT.ETAT%type;
    dadrimm         TIMM2018.ADRIMM%type;
    dnomprop        TCLIENT2018.NOMCLI%type;
    dnblocs         NUMBER;
    dnblocsmsg      TRESULT.NBLOCS%type;
    dnblogs         NUMBER;
    dmessage        VARCHAR2(100);

    dept_inconnu    EXCEPTION;

    CURSOR c_log IS
        SELECT l.REFLOG, l.ETAT, i.ADRIMM, c.NOMCLI
            FROM TLOGT2018 l, TIMM2018 i, TCLIENT2018 c
            WHERE l.NOIMM = i.NOIMM
            AND l.NOPROP = c.NOCLI
            AND i.DEPTIMM = '&vdeptimm';

BEGIN

    dnblogs := 0;
    
    SELECT COUNT(DEPTIMM)
        INTO dcheckdept
        FROM TIMM2018
        WHERE DEPTIMM = '&vdeptimm';

    IF dcheckdept = 0
    THEN
    	dmessage := 'Departement inconnu.';
        RAISE dept_inconnu;
    ELSE
    	INSERT INTO TLIGNE VALUES('Numero departement : ' || TO_CHAR('&vdeptimm'));
    END IF;

    OPEN c_log;
    FETCH c_log INTO dreflog, detat, dadrimm, dnomprop;

    WHILE c_log%FOUND LOOP
        SELECT COUNT(*)
            INTO dnblocs
            FROM TLOCATION2018
            WHERE REFLOG = dreflog
            AND (
                DEBLOC >= '01/01/2016'
                OR (FINLOC >= '01/01/2016' OR FINLOC IS NULL)
            );
            --AND DEBLOC > (SELECT SYSDATE - INTERVAL '2' YEAR FROM DUAL);

        IF detat = 'L' THEN
            dnewEtat := 'Loue';
        ELSE
            IF detat = 'D' THEN
                dnewEtat := 'Disponible';
            ELSE -- dEtat = 'P'
                dnewEtat := 'Utilise';
            END IF;
        END IF;

        IF dnblocs = 0 THEN
            dnblocsmsg := 'Aucun locataire depuis 2 ans.';
        ELSE
            dnblocsmsg := TO_CHAR(dnblocs);
        END IF;

        INSERT INTO TRESULT
            VALUES(dreflog, dnewEtat, dadrimm, dnomprop, dnblocsmsg);

        
        dnblogs := dnblogs + 1;
        FETCH c_log INTO dreflog, detat, dadrimm, dnomprop;
    END LOOP;
    CLOSE c_log;

    IF dnblogs > 0 THEN
        INSERT INTO TLIGNE VALUES('Nombre de logements : ' || dnblogs);
    ELSE
        INSERT INTO TLIGNE VALUES('Aucun logement disponible dans ce departement.');
    END IF;


EXCEPTION
    WHEN dept_inconnu THEN
        INSERT INTO TLIGNE VALUES(dmessage);

END;

---------------------------------------- Terminal output ----------------------------------------
/*
================================================================================
Cas où le logement est connu, la liste s'affiche.
================================================================================
LIGNE
--------------------------------------------------------------------------------------------------------------------------------------------
Numero departement : 63

REFL ETAT               ADRIMM                          NOMPROP            NBLOCS
---- -------------------- ---------------------------------------- ------------------------------ ----------------------------------------
L001 Disponible         Bd Lafayette Clermont Fd         Dupond             Aucun locataire depuis 2 ans.
L002 Loue               Bd Lafayette Clermont Fd         Dupond             2
L004 Utilise            Bd Lafayette Clermont Fd         Thibaut            1
L005 Loue               Bd Lafayette Clermont Fd         Thibaut            1

LIGNE
--------------------------------------------------------------------------------------------------------------------------------------------
Nombre de logements : 4
*/

/*
================================================================================
Cas où le logement est inconnu.
================================================================================
Saisir le numero departement ?
98

LIGNE
--------------------------------------------------------------------------------------------------------------------------------------------
Departement inconnu.
*/

/*
================================================================================
Cas où le logement ne contient aucun logement (département créé préalablement).
================================================================================
Saisir le numero departement ?
91

LIGNE
--------------------------------------------------------------------------------------------------------------------------------------------
Numero departement : 91

LIGNE
--------------------------------------------------------------------------------------------------------------------------------------------
Aucun logement disponible dans ce departement.
*/

.
/

SELECT * FROM TLIGNE WHERE LIGNE LIKE 'Numero%';
SELECT * FROM TRESULT;
SELECT * FROM TLIGNE WHERE LIGNE LIKE '%logement%';
SELECT * FROM TLIGNE WHERE LIGNE LIKE '%inconnu%';

set verify on;
set feed on;
set echo on;