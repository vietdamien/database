---------------------------------------- Exercice 1 ----------------------------------------
DROP TABLE TLIGNE;
CREATE TABLE TLIGNE(LIGNE VARCHAR2(500));

DROP TABLE TRESULT;
CREATE TABLE TRESULT
(
	REFLOG		CHAR(4),
	SUPERF		NUMBER(4),
	LOYER		NUMBER(10,2)
);

set echo off;
set verify off;
set feed off;

-- Result formatting
COLUMN REFLOG FORMAT A15;
COLUMN NUMERO FORMAT A6;
COLUMN REFLOG HEADING 'REF LOGEMENT';

-- @../part1.sql

VARIABLE vnoimm CHAR(4)
PROMPT Saisir le numero immeuble ?
ACCEPT vnoimm

DECLARE
	
	dreflog			TLOGT2018.REFLOG%type;
	dsuperf			TLOGT2018.SUPERF%type;
	dloyer			TLOGT2018.LOYER%type;
	detat			TLOGT2018.ETAT%type;

	dnoimm			TIMM2018.NOIMM%type;
	dadrimm			TIMM2018.ADRIMM%type;

	dligne			TLIGNE.LIGNE%type;

	dnb				NUMBER;

	pas_immeuble	EXCEPTION;
	pas_louable		EXCEPTION;

	CURSOR c IS
		SELECT REFLOG, SUPERF, LOYER, ETAT
		FROM TLOGT2018
		WHERE NOIMM = '&vnoimm';

BEGIN

	SELECT COUNT(*)
		INTO dnb
		FROM TIMM2018
		WHERE NOIMM = '&vnoimm';

	IF dnb > 0 THEN
		SELECT NOIMM NUMERO, ADRIMM ADRESSE
			INTO dnoimm, dadrimm
			FROM TIMM2018
			WHERE NOIMM = '&vnoimm';
		INSERT INTO TLIGNE
			VALUES('Immeuble : ' || TO_CHAR(dnoimm) || '    ' || dadrimm);

		OPEN c;
		FETCH c INTO dreflog, dsuperf, dloyer, detat;

		IF detat = 'D' THEN
			WHILE c%FOUND LOOP
				INSERT INTO TRESULT
					VALUES(dreflog, dsuperf, dloyer);
				FETCH c INTO dreflog, dsuperf, dloyer, detat;
			END LOOP;
		ELSE
			RAISE pas_louable;
		END IF;

		CLOSE c;
	ELSE
		RAISE pas_immeuble;
	END IF;

EXCEPTION
	WHEN pas_immeuble THEN
		INSERT INTO TLIGNE
			VALUES('Numero immeuble inconnu !');
	WHEN pas_louable THEN
		INSERT INTO TLIGNE
			VALUES('Location impossible pour cet immeuble !');

END;

---------------------------------------- Terminal output ----------------------------------------
/*
================================================================================
Cas où il y a des logements de disponibles.
================================================================================
Saisir le numero immeuble ?
I001

LIGNE
--------------------------------------------------------------------------------
Immeuble : I001    Bd Lafayette Clermont Fd

REF LOGEMENT	    SUPERF	LOYER
--------------- ---------- ----------
L001			90	  450
L002			60	  400
L005			90	  470
*/

/*
================================================================================
Cas où tous les logements sont en location.
================================================================================
Saisir le numero immeuble ?
I002

LIGNE
--------------------------------------------------------------------------------
Immeuble : I002    Bd de Courtais Montlucon

LIGNE
--------------------------------------------------------------------------------
Location impossible pour cet immeuble !
*/

/*
================================================================================
Cas où le numéro d'immeuble est inconnu.
================================================================================
Saisir le numero immeuble ?
I999

LIGNE
--------------------------------------------------------------------------------
Numero immeuble inconnu !
*/

.
/

SELECT * FROM TLIGNE;
SELECT * FROM TRESULT;

set verify on;
set feed on;
set echo on;
