---------------------------------------- Exercice 2 ----------------------------------------
DROP TABLE TLIGNE;
CREATE TABLE TLIGNE(LIGNE VARCHAR2(500));

DROP TABLE TRESULT;
CREATE TABLE TRESULT
(
    DEPTIMM     CHAR(2),
    NOIMM       CHAR(4),
    REFLOG      CHAR(4),
    ETAT        CHAR(1)
);

set echo off;
set verify off;
set feed off;

-- @../part1.sql

VARIABLE vnoprop    CHAR(6)
PROMPT Saisir le numero du proprietaire ?
ACCEPT vnoprop
DECLARE

    ddepartement    TIMM2018.DEPTIMM%type;
    dnoimm          TIMM2018.NOIMM%type;
    dreflog         TLOGT2018.REFLOG%type;
    detat           TLOGT2018.ETAT%type;

    dnomcli         TCLIENT2018.NOMCLI%type;

    dcpt            NUMBER;

    CURSOR c IS
       	SELECT i.DEPTIMM, i.NOIMM, l.REFLOG, l.ETAT
            FROM TIMM2018 i, TLOGT2018 l
            WHERE l.NOPROP = '&vnoprop'
            ORDER BY DEPTIMM, NOIMM ASC;

BEGIN

    SELECT NOMCLI
        INTO dnomcli
        FROM TCLIENT2018
        WHERE NOCLI = '&vnoprop';
    INSERT INTO TLIGNE VALUES('Proprietaire : ' || '&vnoprop' || '    ' || dnomcli);
    
    dcpt := 1;
    OPEN c;
    FETCH c INTO ddepartement, dnoimm, dreflog, detat;

    WHILE c%FOUND LOOP
        INSERT INTO TRESULT
            VALUES(ddepartement, dnoimm, dreflog, detat);
        dcpt := dcpt + 1;
    	FETCH c INTO ddepartement, dnoimm, dreflog, detat;
    END LOOP;

    CLOSE c;

    INSERT INTO TLIGNE VALUES('Nombre de logements possedes : ' || TO_CHAR(dcpt));

END;

---------------------------------------- Terminal output ----------------------------------------
/*
Saisir le numero du proprietaire ?
CL0001

LIGNE
--------------------------------------------------------------------------------
Proprietaire : CL0001	 Dupond

DE NOIM REF LOGEMENT	E
-- ---- --------------- -
03 I002 L002		L
03 I002 L001		D
63 I001 L001		D
63 I001 L002		L
63 I003 L002		L
63 I003 L001		D

LIGNE
--------------------------------------------------------------------------------
Nombre de logements possedes : 7
*/

.
/

SELECT * FROM TLIGNE WHERE LIGNE LIKE 'Proprietaire :%';
SELECT * FROM TRESULT;
SELECT * FROM TLIGNE WHERE LIGNE LIKE 'Nombre%';

set verify on;
set feed on;
set echo on;

