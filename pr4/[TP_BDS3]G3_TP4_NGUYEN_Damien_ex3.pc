/*
 * Pro*C
 * TP4 - Exercice 3
 *
 * Auteur : Damien NGUYEN
 * Groupe : 3
 *
 * Notes :
 *      La première question a été compactée pour éviter la duplication de code.
 */

#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

EXEC SQL INCLUDE SQLCA.H;
EXEC SQL INCLUDE SQLDA.H;
EXEC SQL INCLUDE ORACA.H;

#define CLEAR       system("clear")

/********** Constantes **********/
#define SQL_COMMIT      1
#define STOP            1
#define CONTINUE        0
#define SQL_ROLLBACK    0
#define SQL_SUCCESS     0
#define NOT_FOUND       1403

/********** Liste des fonctions **********/
void connexion(void);
void erreur_sql(int arret);
void deconnexion(int validation);
/*
 * Fonction de connexion à Oracle.
 *
 * Les identifiants sont rentrés en dur pour ne pas avoir à les retaper
 * à la main à chaque test, mais dans l'idéal, on fait comme dans la
 * fonction connexionAvecIdentifiants().
 */
void connexion(void) {
    VARCHAR uid[50];
    char identifiants[30] = "danguyen1/danguyen1@kirov";
    printf("Connexion avec les identifiants suivants : %s .\n", identifiants);
    strcpy(uid.arr, identifiants);
    uid.len = strlen(uid.arr);

    EXEC SQL CONNECT :uid;
    if (sqlca.sqlcode == SQL_SUCCESS) {
        printf("Connexion réussie avec succès !\n\n");
    }
    else {
        printf("Connexion échouée !\n\n");
        exit(EXIT_FAILURE);
    }
}

/*
 * Fonction qui affiche les code et message d'erreur SQL.
 *
 * Paramètres :
 *      arret       STOP(1) pour quitter, n'importe quoi pour continuer
 */
void erreur_sql(int arret) {
    printf("Code d'erreur : %d.\n", sqlca.sqlcode);
    printf("Message erreur : %.*s.\n", sqlca.sqlerrm.sqlerrml, sqlca.sqlerrm.sqlerrmc);
    if (arret == STOP) {
        deconnexion(SQL_ROLLBACK);
        exit(EXIT_FAILURE);
    }
}

/*
 * Fonction de déconnexion.
 *
 * Paramètres :
 *      validation  SQL_COMMIT(1) pour COMMIT, n'importe quoi pour ROLLBACK
 */
void deconnexion(int validation) {
    if (validation == SQL_COMMIT) {
        EXEC SQL COMMIT WORK RELEASE;
    }
    else {
        EXEC SQL ROLLBACK WORK RELEASE;
    }
    printf("Déconnexion réussie, travail %s.\n", validation == 1 ? "enregistré" : "annulé");
}

/*
 * Fonction principale.
 *
 * Paramètres :
 *      argc        Le nombre d'arguments
 *      argv        Le tableau d'arguments
 *
 * Retourne :
 *      EXIT_SUCCESS(0) en cas de succès, EXIT_FAILURE sinon
 */
int main(int argc, char const *argv[]) {
    int validation;

    CLEAR;

    connexion();

    EXEC SQL ALTER SESSION SET NLS_DATE_FORMAT='DD/MM/YYYY';
    if (sqlca.sqlcode < SQL_SUCCESS) {
        erreur_sql(STOP);
    }

    EXEC SQL INSERT INTO TABESSAI VALUES('T0001', 150, 'c', 'essai', '15/02/2016');
    if (sqlca.sqlcode < SQL_SUCCESS) {
        erreur_sql(STOP);
    }

    EXEC SQL COMMIT;
    if (sqlca.sqlcode < SQL_SUCCESS) {
        erreur_sql(STOP);
    }

    printf("Insertion dans TABESSAI réussie.\n");

    printf("Enregistrer le travail ? (1 = oui, * = non)\n");
    scanf("%d%*c", &validation);
    printf("Appel de la fonction de déconnexion avec %s du travail.\n", validation == 1 ? "enregistrement" : "annulation");
    deconnexion(validation);

    return EXIT_SUCCESS;
}

/****************************************************************************************************
Cas où on fait n'importe quoi
****************************************************************************************************
danguyen1@iutclinfa1910:~/S2/Database/pr4$ ./ex3

Connexion avec les identifiants suivants : danguyen1/danguyen1@kirov .
Connexion réussie avec succès !

Insertion dans TABESSAI réussie.
Enregistrer le travail ? (1 = oui, * = non)
1
Appel de la fonction de déconnexion avec enregistrement du travail.
Déconnexion réussie, travail enregistré.
****************************************************************************************************
Sous SQLPLUS
****************************************************************************************************
SQL> SELECT * FROM TABESSAI;

ID       NUM U CHAINE       UNEDATE
----- ---------- - -------------------- -------------------
T0001        150 c essai        15/02/2016 00:00:00
****************************************************************************************************/
