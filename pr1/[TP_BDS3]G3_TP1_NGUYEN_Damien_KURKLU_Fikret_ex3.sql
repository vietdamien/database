---------------------------------------- Exercice 3 ----------------------------------------
DROP TABLE TLIGNE;
CREATE TABLE TLIGNE(LIGNE VARCHAR2(500));

set echo off;
set verify off;
set feed off;

ALTER SESSION SET NLS_DATE_FORMAT='DD/MM/YYYY';

-- start ../part1.sql

VARIABLE vnouveaunoloc          CHAR(6)
VARIABLE vdatechangement        DATE
VARIABLE vreflog                CHAR(4)

PROMPT Numero du nouveau locataire ?
ACCEPT vnewnoloc
PROMPT Saisir la date de changement (format : jj/mm/aaaa)
ACCEPT vdatechangement
PROMPT Reference logement ?
ACCEPT vreflog

DECLARE

	dreflog		TLOGT2018.REFLOG%type;
	dnewnoloc	TCLIENT2018.NOCLI%type;
	doldnoloc	TCLIENT2018.NOCLI%type;
	dmessage    VARCHAR2(500);

BEGIN

	dmessage := 'Reference de logement inconnue.';
	SELECT REFLOG
	INTO dreflog
	FROM TLOGT2018
	WHERE REFLOG = '&vreflog';

	dmessage := 'Locataire inconnu.';
	SELECT NOLOC
	INTO doldnoloc
	FROM TLOCATION2018
	WHERE REFLOG = '&vreflog'
	AND FINLOC IS NULL;

	dmessage := 'Client inconnu.';
	SELECT NOCLI
	INTO dnewnoloc
	FROM TCLIENT2018
	WHERE NOCLI = '&vnewnoloc';

	UPDATE TLOCATION2018
	SET FINLOC = '&vdatechangement'
	WHERE FINLOC IS NULL
	AND REFLOG = '&vreflog';

	INSERT INTO TLOCATION2018
	VALUES('&vreflog', '&vdatechangement', NULL, '&vnewnoloc');

	INSERT INTO TLIGNE VALUES('Depart du locataire nr. ' || TO_CHAR(doldnoloc) || ' du logement nr. ' || TO_CHAR(dreflog) || ' enregistre.');
	INSERT INTO TLIGNE VALUES('Arrivee du locataire nr. ' || TO_CHAR(dnewnoloc) || ' du logement nr. ' || TO_CHAR(dreflog) || ' enregistree.');

	EXCEPTION
		WHEN NO_DATA_FOUND THEN
			INSERT INTO TLIGNE VALUES(dmessage);

END;


---------------------------------------- Terminal output ----------------------------------------
/*
================================================================================
Cas où tout se passe bien.
================================================================================
Numero du nouveau locataire ?
CL0003
Saisir la date de changement (format : jj/mm/aaaa)
12/02/2018
Reference logement ?
L003

LIGNE
--------------------------------------------------------------------------------
Depart du locataire nr. CL0004 du logement nr. L003 enregistre.
Arrivee du locataire nr. CL0003 du logement nr. L003 enregistree.
*/

/*
================================================================================
Cas où il n'y a personne dans le logement donné.
================================================================================
Numero du nouveau locataire ?
CL0002
Saisir la date de changement (format : jj/mm/aaaa)
12/05/2018
Reference logement ?
L001

LIGNE
--------------------------------------------------------------------------------
Locataire inconnu.
*/

/*
================================================================================
Cas où le nouveau client entré est incorrect.
================================================================================
Numero du nouveau locataire ?
CL9999
Saisir la date de changement (format : jj/mm/aaaa)
12/05/2018
Reference logement ?
L002

LIGNE
--------------------------------------------------------------------------------
Client inconnu.
*/

.
/

SELECT * FROM TLIGNE;

set verify on;
set feed on;
set echo on;
