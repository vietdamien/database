---------------------------------------- Exercice 1 ----------------------------------------
DROP TABLE TLIGNE;
CREATE TABLE TLIGNE(LIGNE VARCHAR2(500));

-- start ../part1.sql

set echo off;
set verify off;
set feed off;

VARIABLE vreflog CHAR(4)
PROMPT Reference logement ? 
ACCEPT vreflog

DECLARE

	dloyer		TLOGT2018.LOYER%type;
	dsuperf		TLOGT2018.SUPERF%type;
	dnoloc		TLOGT2018.NOLOC%type;
	dnomloc		TCLIENT2018.NOMCLI%type;
	dnblog		NUMBER(4);

BEGIN

	SELECT COUNT(*)
		INTO dnblog
		FROM TLOGT2018
		WHERE REFLOG LIKE '&vreflog';

	IF dnblog != 0 THEN
		SELECT SUPERF, LOYER, NOLOC
			INTO dsuperf, dloyer, dnoloc
			FROM TLOGT2018
			WHERE REFLOG LIKE '&vreflog';

		IF dnoloc != 'NULL' THEN
			SELECT NOMCLI
				INTO dnomloc
				FROM TCLIENT2018
				WHERE NOCLI = dnoloc;
		ELSE
			dnomloc := 'Locataire inconnu';
			
		END IF;

		INSERT INTO TLIGNE VALUES('Superficie : ' || TO_CHAR(dsuperf) || ' Loyer : ' || TO_CHAR(dloyer) || ' Locataire : ' || TO_CHAR(dnomloc));

	ELSE
		INSERT INTO TLIGNE VALUES('Logement inconnu');
		
	END IF;

END;

---------------------------------------- Terminal output ----------------------------------------
/*
================================================================================
Cas où il n'y a pas de logement en cours.
================================================================================
Reference logement ?
L001

LIGNE
--------------------------------------------------------------------------------
Superficie : 90 Loyer : 450 Locataire : Locataire inconnu
*/

/*
================================================================================
Cas où il y a un locataire.
================================================================================
Reference logement ?
L002

LIGNE
--------------------------------------------------------------------------------
Superficie : 60 Loyer : 400 Locataire : Martin
*/

/*
================================================================================
Cas où le logement n'existe pas.
================================================================================
Reference logement ?
L999

LIGNE
--------------------------------------------------------------------------------
Logement inconnu
*/

.
/

SELECT * FROM TLIGNE;

set verify on;
set feed on;
set echo on;
