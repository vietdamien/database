---------------------------------------- Exercice 2 ----------------------------------------
DROP TABLE TLIGNE;
CREATE TABLE TLIGNE(LIGNE VARCHAR2(500));

set echo off;
set verify off;
set feed off;

-- start ../part1.sql

VARIABLE vnoloc CHAR(6)
PROMPT Numero client ?
ACCEPT vnoloc

DECLARE

    dreflog     TLOGT2018.REFLOG%type;
    dloyer      TLOGT2018.REFLOG%type;
    dmessage    VARCHAR2(500);

BEGIN

    SELECT REFLOG, LOYER
        INTO dreflog, dloyer
        FROM TLOGT2018
        WHERE NOLOC = '&vnoloc';

    dmessage := 'Client : ' || TO_CHAR(:vnoloc) || ' Reference : ' || TO_CHAR(dreflog) || ' Loyer : ' || TO_CHAR(dloyer);

    INSERT INTO TLIGNE VALUES(dmessage);

    EXCEPTION
        WHEN NO_DATA_FOUND THEN
            dmessage := 'Pas de location.';
            INSERT INTO TLIGNE VALUES(dmessage);
        WHEN TOO_MANY_ROWS THEN
            dmessage := 'Plusieurs locations.';
            INSERT INTO TLIGNE VALUES(dmessage);


END;

---------------------------------------- Terminal output ----------------------------------------
/*
================================================================================
Cas où il n'y a pas de location.
================================================================================
Numero client ?
CL0001

LIGNE
--------------------------------------------------------------------------------
Pas de location.
*/

/*
================================================================================
Cas où il y a plusieurs locations.
================================================================================
Numero client ?
CL0002

LIGNE
--------------------------------------------------------------------------------
Plusieurs locations
*/

/*
================================================================================
Cas où il n'y a qu'une location.
================================================================================
Numero client ?
CL0004

LIGNE
--------------------------------------------------------------------------------
Client :  Reference : L003 Loyer : 320
*/


.
/

SELECT * FROM TLIGNE;

set verify on;
set feed on;
set echo on;
